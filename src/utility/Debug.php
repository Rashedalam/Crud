<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 4/27/2018
 * Time: 10:51 AM
 */

namespace Rashed\utility;


class Debug
{
    public static function d($vars){
        echo "<pre>";
        print_r($vars);
        echo "</pre>";
    }

    public static function dd($vars){
        self::d($vars);
        die();
        exit();
    }
}