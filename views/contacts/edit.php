<?php
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "Pondit" . DIRECTORY_SEPARATOR . "Crud" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use Rashed\utility\Debug;

//connect to database
$dbh = new PDO("mysql:host=localhost;dbname=crud", "root", "");

//prepare query
$query = "SELECT * FROM `contact` WHERE id=" . $_GET['id'];

$result = $dbh->query($query);

foreach ($result as $row) {
    //Debug::dd($row);
}

?>


<h1 class="text-center">Contact Form</h1>

<form action="update.php" method="post">
    <input type="hidden" id="id" name="id" value="<?= $row['Id'] ?>" class="form-control">
    <div class="row form-group">
        <div class="col-md-6">
            <label for="firstname">First Name</label>
            <input type="text" id="firstname" placeholder="Ente your Name here" name="fname"
                   value="<?= $row['First Name'] ?>" class="form-control">
        </div>

        <div class="col-md-6">
            <label for="lastname">Last Name</label>
            <input type="text" id="lastname" placeholder="Enter your last name here" name="lname"
                   value="<?= $row['Last Name'] ?>" class="form-control">
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-6">
            <label for="email">Email</label>
            <input type="email" id="email" placeholder="Ente your email here" name="email" value="<?= $row['Email'] ?>"
                   class="form-control">
        </div>

        <div class="col-md-6">
            <label for="subject">Subject</label>
            <input type="text" id="subject" placeholder="Enter your subject here" name="subject"
                   value="<?= $row['Subject'] ?>" class="form-control">
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-12">
            <label for="message">Comment</label>
            <textarea type="text" id="message" value="<?= $row['Comment'] ?>"
                      placeholder="Enter your comment here" name="comment" class="form-control"></textarea>
        </div>
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-primary">Send Message</button>
    </div>
<?php
    $output=ob_get_contents();
    ob_end_clean();

    $layout=file_get_contents("../layouts/layout.php");
    echo str_replace("|CONTENTS|","$output","$layout");
?>


    <a href="index.php">Click here</a>to Edit contact information Into Database.