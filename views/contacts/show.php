<?php
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "Pondit" . DIRECTORY_SEPARATOR . "Crud" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use Rashed\utility\Debug;

//connect to database
$dbh = new PDO("mysql:host=localhost;dbname=Crud", "root", "");

//prepare query
$query = "SELECT * FROM `contact` WHERE id=" . $_GET['id'];

$result = $dbh->query($query);

?>

<h1 class="text-center">Contact List</h1>

<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Full Name</th>
        <th scope="col">Email</th>
        <th scope="col">Subject</th>
        <th scope="col">Comment</th>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($result as $row) {
        ?>
        <tr>
            <td scope="col"><?= $row['Id'] ?></td>
            <td scope="col"><?= $row['First Name'] . " " . $row['Last Name'] ?></td>
            <td scope="col"><?= $row['Email'] ?></td>
            <td scope="col"><?= $row['Subject'] ?></td>
            <td scope="col"><?= $row['Comment'] ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>

    <a href="index.php">Click here</a>to Show contact information Into Database.


<?php
    $output=ob_get_contents();
    ob_end_clean();

    $layout=file_get_contents("../layouts/layout.php");
    echo str_replace("|CONTENTS|","$output","$layout");
?>