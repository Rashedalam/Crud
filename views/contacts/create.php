<?php
    ob_start();
?>

<h1 class="text-center">Contact Form</h1>

<form action="store.php" method="post">
    <div class="row form-group">
        <div class="col-md-6">
            <label for="firstname">First Name</label>
            <input type="text" id="firstname" placeholder="Ente your Name here" name="fname" class="form-control">
        </div>

        <div class="col-md-6">
            <label for="lastname">Last Name</label>
            <input type="text" id="lastname" placeholder="Enter your last name here" name="lname" class="form-control">
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-6">
            <label for="email">Email</label>
            <input type="email" id="email" placeholder="Ente your email here" name="email" class="form-control">
        </div>

        <div class="col-md-6">
            <label for="subject">Subject</label>
            <input type="text" id="subject" placeholder="Enter your subject here" name="subject" class="form-control">
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-12">
            <label for="message">Comment</label>
            <textarea type="text" rows="4" cols="4" id="message" placeholder="Enter your comment here" name="comment"
                      class="form-control"></textarea>
        </div>
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-primary">Send Message</button>
    </div>
</form>

<?php
    $output=ob_get_contents();
    ob_end_clean();

    $layout=file_get_contents("../layouts/layout.php");
    echo str_replace("|CONTENTS|","$output","$layout")

?>


