<?php
session_start();
ob_start();

include_once "../../vendor/autoload.php";

//include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";

use Rashed\utility\Debug;

//prepare query
$query = "SELECT * FROM `contact` ";
//connect to database
$dbh = new PDO("mysql:host=localhost;dbname=Crud", "root", "");
$result = $dbh->query($query);


?>


<h1 class="text-center">Contact List</h1>

<div class="alert alert-success"><?= $_SESSION['message']?></div>

<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Full Name</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($result as $row) { ?>
        <tr>
            <td scope="row"><?php echo $row['Id'] ?></td>
            <td scope="row"><?php echo $row['First Name'] . " " . $row['Last Name'] ?></td>
            <td><?php echo $row['Email'] ?></td>
            <td><a href="show.php?id=<?= $row['Id'] ?>">Show</a>|<a
                        href="edit.php?id=<?= $row['Id'] ?>">Edit</a>|<a
                        href="delete.php?id=<?= $row['Id'] ?>" onclick="confirm('Are You Want to Delete This Item?')">Delete</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php
    $output=ob_get_contents();
    ob_end_clean();

    $layout=file_get_contents("../layouts/layout.php");
    echo str_replace("|CONTENTS|","$output","$layout")
?>
